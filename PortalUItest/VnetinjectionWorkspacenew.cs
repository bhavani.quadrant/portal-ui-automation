﻿using Microsoft.Portal.Framework;
using Microsoft.Portal.TestFramework.Core;
using Microsoft.Portal.TestFramework.Core.Authentication;
using Microsoft.Portal.TestFramework.Core.Controls;
using Microsoft.Portal.TestFramework.Core.Controls.Forms;
using Microsoft.Portal.TestFramework.Core.Controls.Toolbars;
using Microsoft.Portal.TestFramework.Core.Shell;
using Microsoft.Selenium.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace PortalUItest
{
    [TestClass]
    public class VnetinjectionWorkspacenew
    {
        [TestMethod]
        public void CreateWorkspaceForm()
        {
            var portalUri = new Uri(ConfigurationManager.AppSettings["PortalUri"]);

            PortalServer.WaitForServerReady(portalUri);

            // Create a webdriver instance to automate the browser.
            var webDriver = WebDriverFactory.Create();
            webDriver.Manage().Window.Maximize();

            // Create a Portal Authentication class to handle login, note that the portalUri parameter is used to validate that login was successful.
            var portalAuth = new PortalAuthentication(webDriver, portalUri);

            try
            {
                //config#sideLoadingExtension
                // Sign into the portal
                portalAuth.SignInAndSkipPostValidation(userName: ConfigurationManager.AppSettings["userName"], /** The account login to use.  Note Multi Factor Authentication (MFA) is not supported, you must use an account that does not require MFA **/
                password: ConfigurationManager.AppSettings["password"], /** The account password **/
                tenantDomainName: "microsoft.onmicrosoft.com", /** the tenant to login to, set only if you need to login to a specific tenant **/
                query: "feature.canmodifyextensions=true",
                fragment: "create/Microsoft.Databricks" /** The hash fragment, use this to optionally navigate directly to your resource on sign in. **/);
            }
            catch (Exception ex) { }


            //Navigate to Create workspace form secrion
            Portal.CheckAndClickExtensionTrustPrompt(webDriver);
            var portal = Portal.FindPortal(webDriver, false);
       // var button = webDriver.WaitUntil(() => portal.FindElement<SideBarBrowseFlyoutButton>(),"Could not find the form.");
            var form = webDriver.WaitUntil(() => portal.FindElement<FormSection>(), "Could not find the form.");
          
           
            string workspaceName = "Workspace name";
            string Subscription = "Subscription";
            string ResourceGroup = "Resource group";
            string Location = "Region";
            string PricingTier = "Pricing Tier";
            //string CreateNewRG = "Name";
            string ExistingVnet = "Deploy Azure Databricks workspace in your own Virtual Network (VNet)";
           

            try
            {
                //Select Subscription from dropdown
                var subdropdownOptions = webDriver.WaitUntil(() => form.FindFieldByLabel<GroupDropDown>(Subscription),
               string.Format("Could not find the {0} subscription.", Subscription));

                subdropdownOptions.SelectItemByText("Azure Databricks RP BugBash 01");
                webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);

                var resourceGroupOption = webDriver.WaitUntil(() => form.FindFieldByLabel<GroupDropDown>(ResourceGroup),
                                    string.Format("Could not find the {0} resourcegroup.", ResourceGroup));
                //resourceGroupOption.SelectItemByText("mani-rgp-dev1");
                //webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
                var RGDropdown = webDriver.WaitUntil(() => resourceGroupOption.FindElement<ResourceGroupDropDown>(),
                                string.Format("Could not find the {0} resourcegroup.", resourceGroupOption));
                //Create new resource group
                RGDropdown.NewResourceGroup = Convert.ToString("mani-rgp-dev-test2");




                //Enter workspace name
                var workspacenametestbox = webDriver.WaitUntil(() => form.FindFieldByLabel<Textbox>(workspaceName),
                               string.Format("Could not find the {0} textbox.", workspaceName));
                workspacenametestbox.Value = "mani-dev-dbws-02" + Keys.Tab;

                //Select Region
                var locationDropDown = webDriver.WaitUntil(() => form.FindFieldByLabel<GroupDropDown>(Location),
                                  string.Format("Could not find the {0} location.", Location));

                var locationName = locationDropDown.Options.First(x => x.HtmlText.Contains("East US 2"));
                locationDropDown.ResetSelected();
                locationDropDown.SelectItemByText(locationName.HtmlText);

                //Select SKU
                var sku = webDriver.WaitUntil(() => form.FindFieldByLabel<GroupDropDown>(PricingTier),
                               string.Format("Could not find the {0} pricing tier.", PricingTier));
                sku.ResetSelected();
                sku.SelectItemByText("Premium (+ Role-based access controls)");

                string Next = "Review + create";
                var footer = webDriver.WaitUntil(() => portal.FindElements<SimpleButton>(),
                string.Format("Could not find the {0} textbox.", Next));
                footer.ToList()[2].Click();

                //EnableNPIPoption
                string NoPublicIp = "Deploy Azure Databricks workspace with Secure Cluster Connectivity (No Public IP)";
                var npipradiooption = webDriver.WaitUntil(() => form.FindFieldByLabel<OptionPicker>(NoPublicIp),
                               string.Format("Could not find the {0} Radio button.", NoPublicIp));
                npipradiooption.SelectedItem = "Yes";


                //exisitngvnetoptionchoosen
                var vnetradiooption = webDriver.WaitUntil(() => form.FindFieldByLabel<OptionPicker>(ExistingVnet),
                               string.Format("Could not find the {0} textbox.", ExistingVnet));
                vnetradiooption.SelectedItem = "Yes";


                //Select Subscription from dropdown
                string VnetName = "virtual Network";
                var getExistngVNet = webDriver.WaitUntil(() => form.FindFieldByLabel<GroupDropDown>(VnetName),
               string.Format("Could not find the {0} subscription.", VnetName));
                getExistngVNet.SelectItemByText("mani-vnet-01");

                //public Subnet name
                string publicSubnetName = "Public Subnet Name";
                var publicsubnetnametestbox = webDriver.WaitUntil(() => form.FindFieldByLabel<Textbox>(publicSubnetName),
               string.Format("Could not find the {0} textbox.", publicSubnetName));
                publicsubnetnametestbox.Value = "publicsubnet" + Keys.Tab;

                //public Subnet CIDR Range
                string publicSubnetCIDRRange = "Public Subnet CIDR Range";
                var publicsubnetcidrtestbox = webDriver.WaitUntil(() => form.FindFieldByLabel<Textbox>(publicSubnetCIDRRange),
               string.Format("Could not find the {0} textbox.", publicSubnetCIDRRange));
                publicsubnetcidrtestbox.Value = "10.7.5.0/24" + Keys.Tab;

                //private Subnet name
                string privateSubnetName = "Private Subnet Name";
                var privatesubnetnametestbox = webDriver.WaitUntil(() => form.FindFieldByLabel<Textbox>(privateSubnetName),
               string.Format("Could not find the {0} textbox.", privateSubnetName));
                privatesubnetnametestbox.Value = "privatesubnet" + Keys.Tab;

                //public Subnet CIDR Range
                string privateSubnetCIDRRange = "Private Subnet CIDR Range";
                var privatesubnetcidrtestbox = webDriver.WaitUntil(() => form.FindFieldByLabel<Textbox>(privateSubnetCIDRRange),
               string.Format("Could not find the {0} textbox.", publicSubnetCIDRRange));
                privatesubnetcidrtestbox.Value = "10.7.6.0/24" + Keys.Tab;

                //moving to review (nextpage)
                string Next1 = "Review + create";
                var footer1 = webDriver.WaitUntil(() => portal.FindElements<SimpleButton>(),
                string.Format("Could not find the {0} textbox.", Next1, TimeSpan.FromSeconds(30)));
                footer1.ToList()[0].Click();
                Thread.Sleep(30000);
                webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(2);


                //moving to create (nextpage)
                Next1 = "Create";
                 footer1 = webDriver.WaitUntil(() => portal.FindElements<SimpleButton>(),
                string.Format("Could not find the {0} textbox.", Next1));
                footer1.ToList()[0].Click();
                Thread.Sleep(240000);

                var topbar = webDriver.WaitUntil(() => portal.FindElements<TopBar>(), "", TimeSpan.FromMinutes(2));
                var hm = topbar.ToList()[0].GetNotificationsMenu();  //.InnerHtml;
                hm.Click();
                Thread.Sleep(4000);
            }
            catch (Exception ex) { }

            //config#dispose
            // Clean up the webdriver after
           // webDriver.Dispose();
            //config#dispose
        }
    }
}
