﻿using Microsoft.Portal.Framework;
using Microsoft.Portal.TestFramework.Core;
using Microsoft.Portal.TestFramework.Core.Authentication;
using Microsoft.Portal.TestFramework.Core.Controls;
using Microsoft.Portal.TestFramework.Core.Controls.Forms;
using Microsoft.Portal.TestFramework.Core.Controls.Toolbars;
using Microsoft.Portal.TestFramework.Core.Shell;
using Microsoft.Selenium.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace PortalUItest
{
    [TestClass]
    public class WorkspaceDelete
    {
        [TestMethod]
        public void DeleteWorkspaceForm()
        {
            var portalUri = new Uri(ConfigurationManager.AppSettings["PortalUri"]);

            PortalServer.WaitForServerReady(portalUri);

            // Create a webdriver instance to automate the browser.
            var webDriver = WebDriverFactory.Create();
            webDriver.Manage().Window.Maximize();

            // Create a Portal Authentication class to handle login, note that the portalUri parameter is used to validate that login was successful.
            var portalAuth = new PortalAuthentication(webDriver, portalUri);

            try
            {
                //config#sideLoadingExtension
                // Sign into the portal
                portalAuth.SignInAndSkipPostValidation(userName: ConfigurationManager.AppSettings["userName"], /** The account login to use.  Note Multi Factor Authentication (MFA) is not supported, you must use an account that does not require MFA **/
                password: ConfigurationManager.AppSettings["password"], /** The account password **/
                tenantDomainName: "microsoft.onmicrosoft.com", /** the tenant to login to, set only if you need to login to a specific tenant **/
                query: "feature.canmodifyextensions=true",
                fragment: "blade/HubsExtension/BrowseResource/resourceType/Microsoft.Databricks%2Fworkspaces" /** The hash fragment, use this to optionally navigate directly to your resource on sign in. **/);
            }
            catch (Exception ex) { }

            //Navigate to Create workspace form section
            Portal.CheckAndClickExtensionTrustPrompt(webDriver);
            var portal = Portal.FindPortal(webDriver, false);
            string WorkspaceName = "mani-wks-01";

            var search = webDriver.WaitUntil(() => portal.FindElement<Textbox>(), "Could not find the form.");
            search.Value = "mani-wks-01";

            //var searchbox = webDriver.WaitUntil(() => portal.FindElement<Textbox>(), "Could not find the Portal.");
            //searchbox.Value = "ws-bhkadh-01";
            var selectworkspace = webDriver.WaitUntil(() => portal.FindElement<Grid>(), "Could not find the Portal.");
            var openworkspace = selectworkspace.FindCell(WorkspaceName);
            openworkspace.Click();
            Thread.Sleep(1000);
            var bladeDatabrick = portal.FindSingleBladeByTitle(WorkspaceName);

            var deleteButtons = webDriver.WaitUntil(() => bladeDatabrick.FindElements<CommandBarItem>(), "Could not find the form.");

            deleteButtons.ToList()[0].Click();

            Thread.Sleep(5000);


            var bladeDatabrickConfirmMsg = portal.FindSingleBladeByTitle(WorkspaceName);

            var okButton = webDriver.WaitUntil(() => portal.FindElements<SimpleButton>(), "");

            var okBtn = okButton.FirstOrDefault(x => x.Text.Contains("OK"));

            okBtn.Click();
            Thread.Sleep(300000);
            var topbar = webDriver.WaitUntil(() => portal.FindElements<TopBar>(), "", TimeSpan.FromMinutes(2));
            var hm = topbar.ToList()[0].GetNotificationsMenu();  //.InnerHtml;
            hm.Click();
            Thread.Sleep(5000);

            // Clean up the webdriver after
            //  webDriver.Dispose();
            //config#dispose
        }
    }
}
