﻿using Microsoft.Portal.TestFramework.Core;
using Microsoft.Portal.TestFramework.Core.Authentication;
using Microsoft.Portal.TestFramework.Core.Controls;
using Microsoft.Portal.TestFramework.Core.Controls.Forms;
using Microsoft.Portal.TestFramework.Core.Shell;
using Microsoft.Selenium.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace PortalUItest
{
    /// <summary>
    /// Summary description for VnetCreating
    /// </summary>
    [TestClass]
    public class UpdateWorkspace
    {
        [TestMethod]
        public void CreateVnetForm()
        {
            var portalUri = new Uri(ConfigurationManager.AppSettings["PortalUri"]);

            PortalServer.WaitForServerReady(portalUri);

            // Create a webdriver instance to automate the browser.
            var webDriver = WebDriverFactory.Create();
            webDriver.Manage().Window.Maximize();

            // Create a Portal Authentication class to handle login, note that the portalUri parameter is used to validate that login was successful.
            var portalAuth = new PortalAuthentication(webDriver, portalUri);

            try
            {
                //config#sideLoadingExtension
                // Sign into the portal
                portalAuth.SignInAndSkipPostValidation(userName: ConfigurationManager.AppSettings["userName"], /** The account login to use.  Note Multi Factor Authentication (MFA) is not supported, you must use an account that does not require MFA **/
                password: ConfigurationManager.AppSettings["password"], /** The account password **/
                tenantDomainName: "microsoft.onmicrosoft.com", /** the tenant to login to, set only if you need to login to a specific tenant **/
                query: "feature.canmodifyextensions=true", /** Query string to use when navigating to the portal. **/
                fragment: "blade/HubsExtension/BrowseResource/resourceType/Microsoft.Databricks%2Fworkspaces", signedInOnce: false /** The hash fragment, use this to optionally navigate directly to your resource on sign in. **/
               );
            }

            catch (Exception ex)
            {
            }


            //Navigate to Create workspace form section
            Portal.CheckAndClickExtensionTrustPrompt(webDriver);
            var portal = Portal.FindPortal(webDriver, false);


            string dataBrickName = "mani-dev1-ADB-01";
            string searchDataBrickName = "form-label-id-0-for";
            string TagName = "Tags";

            // Searching for Existing Databrick  
            var dataBrickTextSearch = webDriver.WaitUntil(() => portal.FindElement<Textbox>(),
                             string.Format("Could not find the {0} textbox.", searchDataBrickName));

            dataBrickTextSearch.Value = dataBrickName + Keys.Tab;

            Thread.Sleep(10000);

            // Open the Search Result Databrick name  
            var dataBrickLink = webDriver.WaitUntil(() => portal.FindElement(By.LinkText(dataBrickName)),
                             string.Format("Could not find given data brick name {0} .", dataBrickName), TimeSpan.FromSeconds(20));

            dataBrickLink.Click();

            Thread.Sleep(10000);

            // Open the Search Result Databrick name  
            var tagLink = webDriver.WaitUntil(() => portal.FindElement(By.LinkText(TagName)),
                             string.Format("Could not find given data brick name {0} .", TagName), TimeSpan.FromSeconds(20));

            tagLink.Click();

            Thread.Sleep(10000);

            // Update Tag Values

            var blade = portal.FindSingleBladeByTitle(dataBrickName + " | Tags");
            var tagGrid = webDriver.WaitUntil(() => blade.FindElements<Grid>(), "Could not find the form.");
            var tagGridCells = tagGrid.ToList();

            string updateName = "test";
            string updateValue = "name";

            foreach (var gRow in tagGridCells)
            {
                int rowsCount = gRow.Rows.Count();


                var nameRow = gRow.Rows.ToList()[rowsCount - 1].Cells[0].FindElement<Textbox>();
                nameRow.Value = updateName + Keys.Tab; 
                Thread.Sleep(10000);

                var valueRow = gRow.Rows.ToList()[rowsCount - 1].Cells[2].FindElement<Textbox>();
                valueRow.Value = updateValue + Keys.Tab; 
                Thread.Sleep(10000);

                break;
            }

            var applyButton = webDriver.WaitUntil(() => portal.FindElements<SimpleButton>(),
            string.Format("Error While Clicking Apply Button"));

            applyButton.ToList()[2].Click();

            Thread.Sleep(20000);
            var menu = webDriver.WaitUntil(() => portal.FindElement<Menu>(), "Could not find the form.", TimeSpan.FromMinutes(2));
            var Overviewbtn = menu.GetMenuItem("Overview");
            Overviewbtn.Click();
            Thread.Sleep(5000);

            // Clean up the webdriver after
            // webDriver.Dispose();
        }

    }
}
