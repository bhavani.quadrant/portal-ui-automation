﻿using Microsoft.Portal.Framework;
using Microsoft.Portal.TestFramework.Core;
using Microsoft.Portal.TestFramework.Core.Authentication;
using Microsoft.Portal.TestFramework.Core.Controls;
using Microsoft.Portal.TestFramework.Core.Controls.Forms;
using Microsoft.Portal.TestFramework.Core.Shell;
using Microsoft.Selenium.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace PortalUItest
{
    [TestClass]
    public class CRUD
    {
        [TestMethod]
        public void CreateWorkspaceForm()
        {
            var portalUri = new Uri(ConfigurationManager.AppSettings["PortalUri"]);

            PortalServer.WaitForServerReady(portalUri);

            // Create a webdriver instance to automate the browser.
            var webDriver = WebDriverFactory.Create();
            webDriver.Manage().Window.Maximize();

            // Create a Portal Authentication class to handle login, note that the portalUri parameter is used to validate that login was successful.
            var portalAuth = new PortalAuthentication(webDriver, portalUri);

            try
            {
                //config#sideLoadingExtension
                // Sign into the portal
                portalAuth.SignInAndSkipPostValidation(userName: ConfigurationManager.AppSettings["userName"], /** The account login to use.  Note Multi Factor Authentication (MFA) is not supported, you must use an account that does not require MFA **/
                password: ConfigurationManager.AppSettings["password"], /** The account password **/
                tenantDomainName: "microsoft.onmicrosoft.com", /** the tenant to login to, set only if you need to login to a specific tenant **/
                query: "feature.canmodifyextensions=true",
                fragment: "create/Microsoft.Databricks" /** The hash fragment, use this to optionally navigate directly to your resource on sign in. **/);
            }
            catch (Exception ex) { }


            //Navigate to Create workspace form secrion
            Portal.CheckAndClickExtensionTrustPrompt(webDriver);
            var portal = Portal.FindPortal(webDriver, false);

            var form = webDriver.WaitUntil(() => portal.FindElement<FormSection>(), "Could not find the form.");

            string workspaceName = "Workspace name";
            string Subscription = "Subscription";
            string ResourceGroup = "Resource group";
            string Location = "Region";
            string PricingTier = "Pricing Tier";
            string CreateNewRG = "Name";


            string subscriptionValue = "Azure Databricks RP BugBash 01";
            string rgValue = "bhkadh-rg-test";
            string wsNameValue = BaseClass.DynamicStr("bhkadh_ws");
            string regionValue = "Australia Central 2";
            string skuValue = "Premium (+ Role-based access controls)";

            try
            {
                //Select Subscription from dropdown
                var subdropdownOptions = webDriver.WaitUntil(() => form.FindFieldByLabel<GroupDropDown>(Subscription),
               string.Format("Could not find the {0} subscription.", Subscription), new TimeSpan(0, 0, 1000));

                subdropdownOptions.SelectItemByText(subscriptionValue);

                var resourceGroupOption = webDriver.WaitUntil(() => form.FindFieldByLabel<GroupDropDown>(ResourceGroup),
                                    string.Format("Could not find the {0} resourcegroup.", ResourceGroup), new TimeSpan(0, 0, 1000));

                var rgExist = resourceGroupOption.Options.ToList().FirstOrDefault(x => x.Value.Contains(rgValue));
                if (rgExist.Value == rgValue)
                {
                    resourceGroupOption.SelectItemByText(rgValue);
                }
                else
                {
                    var RGDropdown = webDriver.WaitUntil(() => resourceGroupOption.FindElement<ResourceGroupDropDown>(),
                                   string.Format("Could not find the {0} resourcegroup.", resourceGroupOption), new TimeSpan(0, 0, 1000));
                    webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);

                    //Create new resource group
                    RGDropdown.NewResourceGroup = Convert.ToString(rgValue);

                }

                Thread.Sleep(1000);

                //Enter workspace name
                var workspacenametextbox = webDriver.WaitUntil(() => form.FindFieldByLabel<Textbox>(workspaceName),
                               string.Format("Could not find the {0} textbox.", workspaceName), new TimeSpan(0, 0, 1000));
                workspacenametextbox.Value = wsNameValue + Keys.Tab;


                //Select Region
                var locationDropDown = webDriver.WaitUntil(() => form.FindFieldByLabel<GroupDropDown>(Location),
                                  string.Format("Could not find the {0} location.", Location), new TimeSpan(0, 0, 1000));
                webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
                var locationName = locationDropDown.Options.First(x => x.HtmlText.Contains(regionValue));
                locationDropDown.ResetSelected();
                locationDropDown.SelectItemByText(locationName.HtmlText);

                //Select SKU
                var sku = webDriver.WaitUntil(() => form.FindFieldByLabel<GroupDropDown>(PricingTier),
                               string.Format("Could not find the {0} pricing tier.", PricingTier), new TimeSpan(0, 0, 1000));
                webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
                sku.ResetSelected();
                sku.SelectItemByText(skuValue);

                //moving to review (nextpage)
                string NextReviewCreate = "Review + create";
                string NextCreate = "Create";
                var footer2 = webDriver.WaitUntil(() => portal.FindElements<SimpleButton>(),
                string.Format("Could not find the {0} textbox.", NextCreate), new TimeSpan(0, 0, 1000));

                var nextBtnclick = footer2.FirstOrDefault(x => x.Text.Contains("Next"));
                nextBtnclick.Click();

                //EnableNPIP
                string NoPublicIp = "Deploy Azure Databricks workspace with Secure Cluster Connectivity (No Public IP)";
                var npipradiooption = webDriver.WaitUntil(() => form.FindFieldByLabel<OptionPicker>(NoPublicIp),
                string.Format("Could not find the {0} Radio button.", NoPublicIp), new TimeSpan(0, 0, 1000));
                npipradiooption.SelectedItem = "Yes";

                //Thread.Sleep(1000);
                //moving to create (nextpage)

                footer2 = webDriver.WaitUntil(() => portal.FindElements<SimpleButton>(),
                string.Format("Could not find the {0} textbox.", NextReviewCreate));

                var nextCreateBtnclick = footer2.FirstOrDefault(x => x.Text.Contains(NextReviewCreate));
                nextCreateBtnclick.Click();

                Thread.Sleep(20000);
                ////moving to create (nextpage)
                footer2 = webDriver.WaitUntil(() => portal.FindElements<SimpleButton>(),
                string.Format("Could not find the {0} textbox.", NextCreate));
                footer2.ToList()[0].Click();

                var CreateBtnclick = footer2.FirstOrDefault(x => x.Text.Contains(NextCreate));
                CreateBtnclick.Click();

                Thread.Sleep(240000);

                var wsStatus = webDriver.WaitUntil(() => portal.FindElement<Grid>(), "Could not find the grid");

                var wsGrid = webDriver.WaitUntil(() => wsStatus.FindElements<GridCell>(), "Could not find the grid");

                var statusCell = wsGrid.ToList()[3].Text;

                webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);

                if (statusCell == "OK")
                {
                    var gotoResourceBtn = webDriver.WaitUntil(() => portal.FindElements<SimpleButton>(),
                                                                string.Format("Could not find the {0} textbox.", "Go to resource"));
                    gotoResourceBtn.ToList()[0].Click();

                    Thread.Sleep(20000);

                    var bladeDatabrick = portal.FindSingleBladeByTitle(wsNameValue);

                    var menu = webDriver.WaitUntil(() => bladeDatabrick.FindElement<Menu>(), "Could not find the form.", TimeSpan.FromMinutes(2));
                    var tagdMenuItem = menu.GetMenuItem("Tags");
                    tagdMenuItem.Click();
                    webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);

                    var blade = portal.FindSingleBladeByTitle(wsNameValue + " | Tags");
                    var tagGrid = webDriver.WaitUntil(() => blade.FindElements<Grid>(), "Could not find the form.");
                    var tagGridCells = tagGrid.ToList();

                    string updateName = "tag";
                    string updateValue = "1";

                    foreach (var gRow in tagGridCells)
                    {
                        int rowsCount = gRow.Rows.Count();


                        var nameRow = gRow.Rows.ToList()[rowsCount - 1].Cells[0].FindElement<Textbox>();
                        nameRow.Value = updateName + Keys.Tab;
                        Thread.Sleep(1000);

                        var valueRow = gRow.Rows.ToList()[rowsCount - 1].Cells[2].FindElement<Textbox>();
                        valueRow.Value = updateValue + Keys.Tab;
                        Thread.Sleep(1000);

                        break;
                    }

                    var applyButtons = webDriver.WaitUntil(() => portal.FindElements<SimpleButton>(),
                    string.Format("Error While Clicking Apply Button"));

                    var applyBtn = applyButtons.FirstOrDefault(x => x.Text.Contains("Apply"));

                    applyBtn.Click();

                    Thread.Sleep(100000);
                    webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
                    var menuNew = webDriver.WaitUntil(() => bladeDatabrick.FindElement<Menu>(), "Could not find the Menu.");

                    var Overviewbtn = menuNew.GetMenuItem("Overview");
                    Overviewbtn.Click();

                    webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);


                    Thread.Sleep(20000);
                    var listofBututtons = webDriver.WaitUntil(() => bladeDatabrick.FindElements<CommandBarItem>(), "Could not find the Commandbar.");

                    var deleteBtn = listofBututtons.FirstOrDefault(x => x.Text.Contains("Delete"));
                    deleteBtn.Click();

                    Thread.Sleep(5000);

                    var okButton = webDriver.WaitUntil(() => portal.FindElements<SimpleButton>(), "Could not find Delete button");

                    var okBtn = okButton.FirstOrDefault(x => x.Text.Contains("OK"));
                    okBtn.Click();

                    Thread.Sleep(100000);
                    var topbar = webDriver.WaitUntil(() => portal.FindElements<TopBar>(), "Could not find the top bar", TimeSpan.FromMinutes(2));
                    var hm = topbar.ToList()[0].GetNotificationsMenu();
                    hm.Click();
                    Thread.Sleep(5000);

                }

                webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);

            }
            catch (Exception ex)
            {

            }


            //config#dispose
            // Clean up the webdriver after
            //  webDriver.Dispose();
            //config#dispose
        }
    }
}
