﻿using Microsoft.Portal.TestFramework.Core;
using Microsoft.Portal.TestFramework.Core.Authentication;
using Microsoft.Portal.TestFramework.Core.Controls;
using Microsoft.Portal.TestFramework.Core.Controls.Forms;
using Microsoft.Portal.TestFramework.Core.Shell;
using Microsoft.Selenium.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Configuration;
using System.Linq;
using System.Threading;

namespace PortalUItest
{
    [TestClass]
    public class EncryptionWorkspace
    {
        [TestMethod]
        public void EnableEncryption()
        {
            // Get the specified Portal Uri from the configuration file
            var portalUri = new Uri(ConfigurationManager.AppSettings["PortalUri"]);
            
            // Make sure the servers are available
            PortalServer.WaitForServerReady(portalUri);
            //ExtensionsServer.WaitForServerReady(extensionUri);

            // Create a webdriver instance to automate the browser.
            var webDriver = WebDriverFactory.Create();
            webDriver.Manage().Window.Maximize();

            // Create a Portal Authentication class to handle login, note that the portalUri parameter is used to validate that login was successful.
            var portalAuth = new PortalAuthentication(webDriver, portalUri);
       
            try
            {
                portalAuth.SignInAndSkipPostValidation(userName: ConfigurationManager.AppSettings["userName"], /** The account login to use.  Note Multi Factor Authentication (MFA) is not supported, you must use an account that does not require MFA **/
                password: ConfigurationManager.AppSettings["password"], /** The account password **/
                tenantDomainName: "microsoft.onmicrosoft.com", /** the tenant to login to, set only if you need to login to a specific tenant **/
                query: "feature.canmodifyextensions=true", /** Query string to use when navigating to the portal. **/
                fragment: "blade/HubsExtension/BrowseResource/resourceType/Microsoft.Databricks%2Fworkspaces", signedInOnce: false /** The hash fragment, use this to optionally navigate directly to your resource on sign in. **/
               );
            }

            catch (Exception ex)
            {
            }

            //Navigate to Create workspace form section
            Portal.CheckAndClickExtensionTrustPrompt(webDriver);
            var portal = Portal.FindPortal(webDriver, false);
           // var loading = webDriver.Manage().Timeouts().ImplicitWait.Add(TimeSpan.FromSeconds(10000));
       //    webDriver wait = new webDriver(webDriver, 10);
            var wsName = "bhkadh-ws-01";

            var searchBox = webDriver.WaitUntil(() => portal.FindElement<Textbox>(), "Could not find the form.");
            searchBox.Value = wsName + Keys.Tab;

            var grid = webDriver.WaitUntil(() => portal.FindElement<Grid>(), "Could not find the form.", TimeSpan.FromMinutes(1));
            // grid.SelectRow(wsName).Click();
            var openworkspace = grid.FindCell(wsName);
            openworkspace.Click();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);


            var menu = webDriver.WaitUntil(() => portal.FindElement<Menu>(), "Could not find the form.", TimeSpan.FromMinutes(2));
            var encryptionMenuItem = menu.GetMenuItem("Encryption");
            encryptionMenuItem.Click();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);

            var form2 = webDriver.WaitUntil(() => portal.FindElements<Blade>(), "Could not find the form.");
            var partcontent1 = form2.ToList()[1];
            var checkboxlist= webDriver.WaitUntil(() => partcontent1.FindElements<CheckBox>(), "Could not find the form.");
    
            var checkBox = checkboxlist.FirstOrDefault(x => x.Text.Contains("Use your own key"));
            checkBox.Click();

            try
            {

                var keyIdentifier = webDriver.WaitUntil(() => partcontent1.FindElements<Textbox>(), "Could not find the form.");
                keyIdentifier.ToList()[1].Value = "https://bhkadh-kv01.vault.azure.net/keys/Key1/52ae7bf551b448e5ac8d3e503a91a623";

                webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
                var subdropdownOptions = webDriver.WaitUntil(() => partcontent1.FindElement<GroupDropDown>(),
                    string.Format("Could not find the {0} subscription.", "Subscription"));

                subdropdownOptions.SelectItemByText("Azure Databricks RP BugBash 01");

                var saveBtnlitem = webDriver.WaitUntil(() => partcontent1.FindElements<CommandBarItem>(), "Could not find the commandbar item.", TimeSpan.FromMinutes(1));
                Thread.Sleep(20000);
                webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(1);
                
                var saveBtn = saveBtnlitem.FirstOrDefault(x => x.Text.Contains("Save"));
                saveBtn.Click();

                
                Thread.Sleep(20000);
                webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(1);

                var topbar = webDriver.WaitUntil(() => portal.FindElements<TopBar>(), "", TimeSpan.FromMinutes(2));
                var hm = topbar.ToList()[0].GetNotificationsMenu();
                hm.Click();

                //topbar.ToList().
                Thread.Sleep(10000);
                webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);

            }
            catch (Exception ex)
            {
            }


            // Clean up the webdriver after
          //  webDriver.Dispose();
            //config#dispose
        }
    }
}

