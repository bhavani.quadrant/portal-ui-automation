﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalUItest
{
    public static class BaseClass
    {
        public static string DynamicStr(string input)
        {
            DateTime dt = System.DateTime.Now;
            string date = dt.ToString("yyyy_MM_dd");
            string time = System.DateTime.Now.ToString("hh_mm");
            return string.Format("{0}_{1}_{2}", input, date, time);
        }
    }
}
